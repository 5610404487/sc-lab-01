package model;

public class CashCard {
	private int id;
	private String name;
	private int money;

	public CashCard(int id,int money) {
		this.id = id;
		this.money = money;
	}
	

	public void Deposit(int amount){
		money = money + amount;
	}
	public void WithDraw(int amount){
		money = money - amount;
	}
	public void setName(String str) {
		name = str;
	}

	public String toString() {
		return "Name: "+name + "   Amount: " + money;
		
		
	}
	
}
