package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.CashCard;
import gui.SoftwareFrame;

public class SoftwareTest {

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new SoftwareTest();
	}

	public SoftwareTest() {
		frame = new SoftwareFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		/*
		 * Student may modify this code and write your result here
		 * frame.setResult("aaa\t bbb\t ccc\t"); frame.extendResult("ddd");
		 */
		
		CashCard stu1 = new CashCard(4193,200);
		CashCard stu2 = new CashCard(4487,100);
		stu1.setName("Aor");
		stu2.setName("Boong");
		frame.setResult(">>>>>Balance : ");
		frame.extendResult(stu1.toString());
		frame.extendResult(stu2.toString());
		frame.extendResult(">>>>>Deposit : 500");
		stu1.Deposit(500);
		stu2.Deposit(500);
		frame.extendResult(stu1.toString());
		frame.extendResult(stu2.toString());
		frame.extendResult(">>>>>WithDraw : 280");
		stu1.WithDraw(280);
		stu2.WithDraw(280);
		frame.extendResult(stu1.toString());
		frame.extendResult(stu2.toString());
	}

	ActionListener list;
	SoftwareFrame frame;
}
